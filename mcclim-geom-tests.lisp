(defpackage :mcclim-geom-tests
  (:use :geometry :clim :clim-lisp))
(in-package :mcclim-geom-tests)

(defun geom-test-1 ()
  ;; line intersection
  (let ((frame (make-application-frame 'geom-test))
	p1 p2 p3 p4
	l1 l2
	i1)
    (push (setf p1 (make-instance 'geom-point-x-y :x 50         :y 50)) (construction-of frame))
    (push (setf p2 (make-instance 'geom-point-x-y :x (- 256 50) :y 50)) (construction-of frame))
    (push (setf p3 (make-instance 'geom-point-x-y :x (- 256 50) :y (- 256 50))) (construction-of frame))
    (push (setf p4 (make-instance 'geom-point-x-y :x 50         :y (- 256 50))) (construction-of frame))
    (push (setf l1 (make-instance 'geom-line-a-b :a p1 :b p3)) (construction-of frame))
    (push (setf l2 (make-instance 'geom-line-a-b :a p2 :b p4)) (construction-of frame))
    (push (setf i1 (make-instance 'geom-point-line-line-intersection :a l1 :b l2))
	  (construction-of frame))
    (run-frame-top-level frame)))

(defun geom-test-2 ()
  ;; circle and line intersection
  (let ((frame (make-application-frame 'geom-test))
	p1 p2 p3 p4
	c1
	l1
	i1 i2)
    (push (setf p1 (make-instance 'geom-point-x-y :x 128 :y 128)) (construction-of frame))
    (push (setf p2 (make-instance 'geom-point-x-y :x 128 :y (+ 128 60))) (construction-of frame))
    (push (setf p3 (make-instance 'geom-point-x-y :x 80 :y 80)) (construction-of frame))
    (push (setf p4 (make-instance 'geom-point-x-y :x 200 :y 150)) (construction-of frame))
    ;;
    (push (setf c1 (make-instance 'geom-circle-center-radius :center p1 :point-on-circumference p2))
	  (construction-of frame))
    (push (setf l1 (make-instance 'geom-line-a-b :a p3 :b p4))
	  (construction-of frame))
    ;;
    (push (setf i1 (make-instance 'geom-point-circle-line-intersection :circle c1 :line l1))
	  (construction-of frame))
    (push (setf i2 (make-instance 'geom-point-circle-line-intersection :circle c1 :line l1 :other t))
	  (construction-of frame))
    (run-frame-top-level frame)))

(defun geom-test-3 ()
  ;; circle and circle intersection
  (let ((frame (make-application-frame 'geom-test))
	p1 p2 p3 p4
	c1 c2
	i1 i2)
    (push (setf p1 (make-instance 'geom-point-x-y :x (- 128 50) :y 128)) (construction-of frame))
    (push (setf p2 (make-instance 'geom-point-x-y :x 128 :y 128)) (construction-of frame))
    (push (setf p3 (make-instance 'geom-point-x-y :x (+ 128 50) :y 128)) (construction-of frame))
    (push (setf p4 (make-instance 'geom-point-x-y :x 128 :y (+ 128 50))) (construction-of frame))
    ;;
    (push (setf c1 (make-instance 'geom-circle-center-radius :center p1 :point-on-circumference p2))
	  (construction-of frame))
    (push (setf c2 (make-instance 'geom-circle-center-radius :center p3 :point-on-circumference p4))
	  (construction-of frame))
    ;;
    (push (setf i1 (make-instance 'geom-point-circle-circle-intersection :left c1 :right c2))
	  (construction-of frame))
    (push (setf i2 (make-instance 'geom-point-circle-circle-intersection :left c1 :right c2 :other t))
	  (construction-of frame))
    (run-frame-top-level frame)))

(defun geom-test-4 ()
  ;; circle from 3 points
  (let ((frame (make-application-frame 'geom-test))
	p1 p2 p3
	l1 l2
	b1 b2
	n1 n2
	i1
	c1)
    (push (setf p1 (make-instance 'geom-point-x-y :x 20 :y 120)) (construction-of frame))
    (push (setf p2 (make-instance 'geom-point-x-y :x 100 :y 40)) (construction-of frame))
    (push (setf p3 (make-instance 'geom-point-x-y :x 180 :y 90)) (construction-of frame))
    ;;
    (push (setf l1 (make-instance 'geom-line-a-b :a p1 :b p2)) (construction-of frame))
    (push (setf l2 (make-instance 'geom-line-a-b :a p2 :b p3)) (construction-of frame))
    ;;
    (push (setf b1 (make-instance 'geom-point-bisector :left p1 :right p2)) (construction-of frame))
    (push (setf b2 (make-instance 'geom-point-bisector :left p2 :right p3)) (construction-of frame))
    ;;
    (push (setf n1 (make-instance 'geom-line-normal :at b1 :to p1)) (construction-of frame))
    (push (setf n2 (make-instance 'geom-line-normal :at b2 :to p2)) (construction-of frame))
    ;;
    (push (setf i1 (make-instance 'geom-point-line-line-intersection :a n1 :b n2))
	  (construction-of frame))
    ;;
    (push (setf c1 (make-instance 'geom-circle-center-radius :center i1 :point-on-circumference p1))
	  (construction-of frame))
    (run-frame-top-level frame)))

(defclass geom-point () ())
(defclass geom-point-x-y (geom-point)
     ((x :initarg :x :accessor x-of)
      (y :initarg :y :accessor y-of)))
(defclass geom-point-line-line-intersection (geom-point)
     ((a :initarg :a :accessor a-of)
      (b :initarg :b :accessor b-of)))
(defclass geom-point-circle-line-intersection (geom-point)
     ((circle :initarg :circle :accessor circle-of)
      (line :initarg :line :accessor line-of)
      (other :initform nil :initarg :other :accessor other)))
(defclass geom-point-circle-circle-intersection (geom-point)
     ((left :initarg :left :accessor left-of)
      (right :initarg :right :accessor right-of)
      (other :initform nil :initarg :other :accessor other)))
(defclass geom-point-bisector (geom-point)
     ((left :initarg :left :accessor left-of)
      (right :initarg :right :accessor right-of)))

(defgeneric point-x-y (geom-point))

(defmethod point-x-y ((point geom-point-x-y))
  (values (x-of point) (y-of point)))

(defmethod point-x-y ((point geom-point-line-line-intersection))
  (multiple-value-bind (x1 y1) (point-x-y (a-of (a-of point)))
    (multiple-value-bind (x2 y2) (point-x-y (b-of (a-of point)))
      (multiple-value-bind (x3 y3) (point-x-y (a-of (b-of point)))
	(multiple-value-bind (x4 y4) (point-x-y (b-of (b-of point)))
	  (multiple-value-call #'line-intersection
	    x1 y1 x2 y2 x3 y3 x4 y4))))))

(defmethod point-x-y ((point geom-point-circle-line-intersection))
  (multiple-value-bind (x1 y1) (point-x-y (a-of (line-of point)))
    (multiple-value-bind (x2 y2) (point-x-y (b-of (line-of point)))
      (multiple-value-bind (cx cy cr) (center-and-radius (circle-of point))
	(let ((points (multiple-value-list (line-circle-intersection x1 y1 x2 y2 cx cy cr))))
	  (if (other point)
	      (when (= (length points) 4)
		(values (elt points 2) (elt points 3)))
	      (when (>= (length points) 2)
		(values (elt points 0) (elt points 1)))))))))

(defmethod point-x-y ((point geom-point-circle-circle-intersection))
  (multiple-value-bind (c1x c1y c1r) (center-and-radius (left-of point))
    (multiple-value-bind (c2x c2y c2r) (center-and-radius (right-of point))
      (let ((points (multiple-value-list (circle-circle-intersection c1x c1y c1r c2x c2y c2r))))
	(when points
	  (if (other point)
	      (values (elt points 2) (elt points 3))
	      (values (elt points 0) (elt points 1))))))))

(defmethod point-x-y ((point geom-point-bisector))
  (multiple-value-bind (x1 y1) (point-x-y (left-of point))
    (multiple-value-bind (x2 y2) (point-x-y (right-of point))
      (multiple-value-call #'lerp2 x1 y1 x2 y2 0.5))))

(defclass geom-line () ())
(defclass geom-line-a-b (geom-line)
     ((a :initarg :a :accessor a-of)
      (b :initarg :b :accessor b-of)))
(defclass geom-line-normal (geom-line)
     ((at :initarg :at :accessor normal-start)
      (to :initarg :to :accessor normal-reference)))

(defmethod a-of ((line geom-line-normal))
  (normal-start line))

(defmethod b-of ((line geom-line-normal))
 (multiple-value-bind (x1 y1) (point-x-y (normal-start line))
   (multiple-value-bind (x2 y2) (point-x-y (normal-reference line))
     (let ((vx (- x2 x1))
	   (vy (- y2 y1)))
       (make-instance 'geom-point-x-y :x (+ x1 vy) :y (+ y1 (- vx)))))))

(defclass geom-circle () ())
(defclass geom-circle-center-radius (geom-circle)
     ((center-point :initarg :center :accessor center-of)
      (radius-point :initarg :point-on-circumference :accessor circumference-point)))

(defgeneric center-and-radius (circle))

(defmethod center-and-radius ((circle geom-circle-center-radius))
  (multiple-value-bind (x y) (point-x-y (center-of circle))
    (multiple-value-bind (cx cy) (point-x-y (circumference-point circle))
      (values x y (distance x y cx cy)))))

(defgeneric display (object pane))

(defmethod display ((point geom-point) pane)
  (multiple-value-bind (x y) (point-x-y point)
    (ignore-errors
      (draw-circle* pane x y 2)
      (when (typep point 'geom-point-x-y)
	(draw-circle* pane x y 4 :filled nil)))))

(defmethod display ((line geom-line) pane)
  (multiple-value-bind (x1 y1) (point-x-y (a-of line))
    (multiple-value-bind (x2 y2) (point-x-y (b-of line))
      (ignore-errors
	(draw-line* pane x1 y1 x2 y2)))))

(defmethod display ((circle geom-circle) pane)
  (multiple-value-bind (x y r) (center-and-radius circle)
    (ignore-errors
      (draw-circle* pane x y r :filled nil))))

(defclass geom-pane (basic-gadget) ())
(define-application-frame geom-test ()
  ((construction :initform '() :accessor construction-of)
   (selected-point :initform '() :accessor selected-point))
  (:panes (canvas (make-pane 'geom-pane)))
  (:layouts (default (vertically (:width 256 :height 256)
		       canvas))))

(defmethod handle-repaint ((pane geom-pane) region)
  (declare (ignore region))
  (with-bounding-rectangle* (x0 y0 x1 y1) (sheet-region pane)
    (draw-rectangle* pane x0 y0 x1 y1 :filled t :ink (make-rgb-color 1 1 1))
    (mapc #'(lambda (bit) (display bit pane))
	  (construction-of *application-frame*))))

(defmethod handle-event ((pane geom-pane) (event pointer-button-press-event))
  (let ((mouse-x (pointer-event-x event))
	(mouse-y (pointer-event-y event)))
    (dolist (bit (construction-of *application-frame*))
      (when (typep bit 'geom-point-x-y)
	(multiple-value-bind (point-x point-y) (point-x-y bit)
	  (when (< (distance mouse-x mouse-y point-x point-y) 10)
	    (setf (selected-point *application-frame*) bit)
	    (return)))))))

(defmethod handle-event ((pane geom-pane) (event pointer-button-release-event))
  (setf (selected-point *application-frame*) nil))

(defmethod handle-event ((pane geom-pane) (event pointer-motion-event))
  (when (selected-point *application-frame*)
    (let ((mouse-x (pointer-event-x event))
	  (mouse-y (pointer-event-y event)))
      (setf (x-of (selected-point *application-frame*)) mouse-x)
      (setf (y-of (selected-point *application-frame*)) mouse-y))
    (repaint-sheet pane (sheet-region pane))))
